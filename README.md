### autonavi
autonavi 是一款自动化生成文档导航的js插件，基于jquery。
不但可以自动生成单个文档的导航栏
也可以通过传入自动化的参数来生成导航栏
autonavi生成的导航栏是自适应的。适合pc和移动浏览。

[DEMO查看示例](http://lec.gitee.io/autonavi)

pc默认页面下拉自动浮动。
移动默认界面折叠浮动。 :bowtie: 
可以从autonavi的官方网站来获取更详细的文档：[http://www.lemge.com/?_c=index&_m=o&o=autonavi](http://www.lemge.com/?_c=index&_m=o&o=autonavi)

#2016年11月23日更新
增加在html中初始化导航，文档：[http://www.lemge.com/--index-o-o-autonavi.html#2016年11月23日](http://www.lemge.com/--index-o-o-autonavi.html#2016年11月23日)