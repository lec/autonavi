/**
 * Created by Administrator on 2016/10/24.
 */

//这段被注释掉的代码是用来测试向自动目录传递参数的。
//$m=[
//];
//for ($i=0;$i<10;$i++)
//{
//    $m[]=[
//        'text'=>"题目".$i,
//        'level'=>rand(1,3),
//        'link'=>'link'.$i,
//    ];
//}
//$mjson=json_encode($m,JSON_UNESCAPED_UNICODE);
$(document).ready(function(){
    an.init({});
});

$(window).scroll(function(){
    an.set_anvnavi();
});
$(document).on('click','._naviitem',function(){
    an.active_navi_item(this);
});
$(document).on('click','._anvnavi_drawer_mob',function () {
    an.anvnavi_drawer_click();
});

$(window).resize(function(){
    an.set_size();
});

var an={
    ww:{},
    drawer_txt:'目录',
    navidata:{},
    init:function(){
        this.ww=$(window).width();
        var initdata=arguments[0]?arguments[0]:{};
        if(initdata.navidata)
        {
            this.navidata=initdata.navidata;
        }else{
            this.navidata=this.initdata4html();
        }
        this.domh();
        this.set_anvnavi();
    },
    initdata4html:function(){//从html中初始化导航数据
        var tmpjson=[];
        $('._anvnavi-ele').each(function () {
            var level=1;
            if($(this).hasClass('_an1'))
            {
                level=1;
            }else if($(this).hasClass('_an2'))
            {
                level=2;
            }else if($(this).hasClass('_an3'))
            {
                level=3;
            }

            tmpjson.push({
                "text":$(this).text(),
                'level':level,
                'link':$(this).attr('href')
            });
        });
        if(tmpjson.length>0)
        {
            $('._anvnavi-ele-box').each(function () {
                $(this).css('display','none');
            });
        }
        return tmpjson;
    },
    domh:function () {
        var _anvbox=document.createElement('div');
        // $(_anvbox).css('border','1px solid green');
        $(_anvbox).addClass('_anvbox');
        $('._autonavi').each(function(){
            // $(this).css('border','1px solid red');
        });
        $('._autonavi').wrapAll(_anvbox);

        var _anvnavi=document.createElement('div');
        $(_anvnavi).addClass('_anvnavi');
        // $(_anvnavi).css('border','1px solid black');

        var _anvnavi_drawer_mob=document.createElement('div');
        $(_anvnavi_drawer_mob).addClass('_anvnavi_drawer_mob');
        $(_anvnavi_drawer_mob).text(this.drawer_txt);
        $('body').append(_anvnavi_drawer_mob);

        if(!$.isEmptyObject(this.navidata))
        {
            var navidata=this.navidata;
            for(i in navidata)
            {
                var t=navidata[i].text;
                t=$.trim(t);
                var naviitem=document.createElement('a');
                $(naviitem).text(t);
                $(naviitem).addClass('_naviitem');

                var level=navidata[i].level;
                if(level==1){
                    $(naviitem).addClass('_naviitem_h2');
                }else if(level==2){
                    $(naviitem).addClass('_naviitem_h3');
                }else if(level==3){
                    $(naviitem).addClass('_naviitem_h4');
                }

                $(naviitem).attr('href',navidata[i].link);
                $(_anvnavi).append(naviitem);
            }
        }else{
            $('._an').each(function(){
                var t=$(this).text();
                t=$.trim(t);
                var n=$(this).data('navname');
                if(!n)
                {
                    n=t;
                }
                $(this).before('<a name="'+n+'"></a>');
                var naviitem=document.createElement('a');
                $(naviitem).text(t);
                $(naviitem).addClass('_naviitem');

                var tagname=$(this)[0].tagName.toLowerCase();
                if(tagname=='h2'){
                    $(naviitem).addClass('_naviitem_h2');
                }else if(tagname=='h3'){
                    $(naviitem).addClass('_naviitem_h3');
                }else if(tagname=='h4'){
                    $(naviitem).addClass('_naviitem_h4');
                }

                $(naviitem).attr('href',"#"+n);
                $(_anvnavi).append(naviitem);
            });
        }

        $('._anvbox').prepend(_anvnavi);
        $('._anvbox').append("<div class='_navibot'></div>");
    },
    active_navi_item:function (item_obj) {
        $('._naviitem').each(function(){
            $(this).removeClass('_naviitem_active');
        });
        $(item_obj).addClass('_naviitem_active');
        if(this.ww<750)
        {
            this.anvnavi_drawer_click();
        }

    },
    set_anvnavi:function () {
        var anvbox=$('._anvbox:first')[0];
        var anvnavi=$('._anvnavi')[0];


        this.set_size();

        var objxy=anvbox.getBoundingClientRect();
        if(objxy.top<0)
        {
            $(anvnavi).addClass('_anvnavi_flex');
        }else{
            $(anvnavi).removeClass('_anvnavi_flex');
        }
    },
    set_size:function () {
        var anvbox=$('._anvbox:first')[0];
        var anvnavi=$('._anvnavi:first')[0];
        var anvmain=$('._autonavi:first')[0];
        var anvnavi_drawer=$('._anvnavi_drawer_mob:first')[0];


        this.ww=$(window).width();

        $('#_msg').html(this.ww);

        if(this.ww<750)
        {
            $(anvnavi).addClass('_anvnavi_mob');
            $(anvmain).addClass('_anvmain_mob');
            $(anvnavi_drawer).css('display','block');
            $(anvnavi).outerWidth('60%');
        }else{
            $(anvnavi).outerWidth($(anvbox).width()*0.2);
            $(anvnavi_drawer).css('display','none');
            $(anvnavi).removeClass('_anvnavi_mob');
            $(anvmain).removeClass('_anvmain_mob');
        }

    },
    anvnavi_drawer_click:function () {
        var anvnavi=$('._anvnavi:first')[0];
        var anvnavi_drawer=$('._anvnavi_drawer_mob:first')[0];
        if($(anvnavi).css('display')=='none')
        {
            $(anvnavi).css('display','block');
            $(anvnavi_drawer).css('left','60%');
        }else{
            $(anvnavi).css('display','none');
            $(anvnavi_drawer).css('left','0');
        }
    }
}
an.set_size();